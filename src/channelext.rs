use crate::CreateEmbed;
use crate::Embedable;
use core::convert::TryFrom;
use core::future::Future;
use core::mem;
use core::ops::{Deref, DerefMut};
use magic::traits::{MagicOption, MagicStr};
use serde_json::map::Map;
use serde_json::{json, Value};
use serenity::builder::Timestamp;
use serenity::http::client::Http;
use serenity::http::AttachmentType;
use serenity::model::channel::{Message, ReactionType};
use serenity::model::id::{ChannelId, MessageId};
// use serenity::Result;
use rand::prelude::*;
use std::pin::Pin;
use std::sync::Arc;
use std::task::{Context, Poll};

type Result<T> = core::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

type BoxedFuture<'a, T> = Pin<Box<dyn Future<Output = T> + 'a + Send>>;

pub trait SerenityHttp {
    fn http(&self) -> Arc<Http>;
}

impl<T: AsRef<Arc<Http>>> SerenityHttp for T {
    fn http(&self) -> Arc<Http> {
        Arc::clone(self.as_ref())
    }
}

pub trait ChannelExt: Into<ChannelId> + Clone {
    #[inline]
    fn send_message<'a>(&self, http: impl SerenityHttp) -> SendMessageBuilder<'a> {
        SendMessageBuilder::new(http.http(), self.to_owned().into())
    }

    #[inline]
    fn send_embed<'a>(&self, http: impl SerenityHttp) -> SendEmbedBuilder<'a> {
        SendEmbedBuilder::new(http.http(), self.to_owned().into())
    }

    fn edit_message<'a>(&self, http: impl SerenityHttp, mess: MessageId) -> EditMessageBuilder<'a> {
        EditMessageBuilder::new(http.http(), self.to_owned().into(), mess)
    }
}

impl<C: Into<ChannelId> + Clone> ChannelExt for C {}

pub struct SendMessageBuilder<'a> {
    http: Option<Arc<Http>>,
    channel: u64,
    content: Option<Map<String, Value>>,
    content_overflow: u16,
    // embed_overflow: u16,
    reactions: Option<Vec<ReactionType>>,
    attachments: Option<Vec<AttachmentType<'a>>>,
    fut: Option<BoxedFuture<'a, Result<Message>>>,
}

impl<'a> SendMessageBuilder<'a> {
    pub fn new(http: Arc<Http>, channel: ChannelId) -> Self {
        Self {
            http: Some(http),
            channel: channel.0,
            content: Some(Default::default()),
            fut: None,
            attachments: None,
            reactions: None,
            content_overflow: 0,
        }
    }

    pub fn with_content(mut self, content: impl ToString) -> Self {
        let content = content.to_string();
        let len = u16::try_from(content.count()).unwrap_or(u16::MAX);

        if len > 2000 {
            self.content_overflow = len - 2000;
        } else {
            self.content_overflow = 0;
            self.content
                .get_or_insert_with(Map::new)
                .insert(String::from("content"), content.to_string().into());
        }

        self
    }

    pub fn with_file(mut self, file: impl Into<AttachmentType<'a>>) -> Self {
        self.attachments.extend_inner(file.into());
        self
    }

    pub fn with_files<F: Into<AttachmentType<'a>>, I: IntoIterator<Item = F>>(
        mut self,
        files: I,
    ) -> Self {
        for attachment in files {
            self.attachments.extend_inner(attachment.into());
        }

        self
    }

    pub fn with_embed(mut self, obj: impl Embedable) -> Self {
        let mut embed = CreateEmbed::default();

        obj.append_to(&mut embed);

        let embed = serenity::utils::hashmap_to_json_map(embed.0);
        self.content
            .get_or_insert_with(Map::new)
            .insert(String::from("embed"), embed.into());

        self
    }

    pub fn with_reactions<R: Into<ReactionType>, I: IntoIterator<Item = R>>(
        mut self,
        reactions: I,
    ) -> Self {
        for reaction in reactions {
            self.reactions.extend_inner(reaction.into());
        }

        self
    }

    pub fn is_tts(mut self, tts: bool) -> Self {
        self.content
            .get_or_insert_with(Map::new)
            .insert(String::from("tts"), tts.into());
        self
    }
}

impl<'a> Future for SendMessageBuilder<'a> {
    type Output = Result<Message>;

    fn poll(mut self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        match &mut self.fut {
            Some(ref mut f) => f.as_mut().poll(ctx),
            None => {
                let http = self.http.take().unwrap();
                let mut content = self.content.take().unwrap();

                if self.attachments.is_some() {
                    if let Some(e) = content.remove("embed") {
                        let name = String::from("payload_json");
                        if let Some(c) = content.remove("content") {
                            content.insert(name, json!({ "content": c, "embed": e }));
                        } else {
                            content.insert(name, json!({ "embed": e }));
                        }
                    }
                }

                let attachments = self.attachments.take();
                let channel = self.channel;
                let reactions = self.reactions.to_owned();

                self.fut = Some(Box::pin(async move {
                    let message = match attachments {
                        Some(a) => http.send_files(channel, a, content).await?,
                        None => http.send_message(channel, &content.into()).await?,
                    };

                    if let Some(reactions) = &reactions {
                        for reaction in reactions {
                            http.create_reaction(channel, message.id.0, &reaction)
                                .await?;
                        }
                    }

                    Ok(message)
                }));

                self.fut.as_mut().unwrap().as_mut().poll(ctx)
            }
        }
    }
}

pub struct SendEmbedBuilder<'a> {
    http: Option<Arc<Http>>,
    channel: u64,
    embed: CreateEmbed,
    fut: Option<BoxedFuture<'a, Result<Message>>>,
}

impl Deref for SendEmbedBuilder<'_> {
    type Target = CreateEmbed;

    fn deref(&self) -> &Self::Target {
        &self.embed
    }
}

impl DerefMut for SendEmbedBuilder<'_> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.embed
    }
}

pub trait Color {
    fn to_number(&self) -> u32;
}

impl Color for u32 {
    fn to_number(&self) -> u32 {
        *self
    }
}

macro_rules! impl_color {
    ($($x:ident),*) => {
        $(
            impl Color for $x {
                fn to_number(&self) -> u32 {
                    *self as u32
                }
            }
        )*
    }
}

impl_color! {
    i32, i64, i128, u64, u128
}

impl Color for (u8, u8, u8) {
    fn to_number(&self) -> u32 {
        (self.0 as u32) << 16 | (self.1 as u32) << 8 | self.2 as u32
    }
}

impl Color for [u8; 3] {
    fn to_number(&self) -> u32 {
        (self[0] as u32) << 16 | (self[1] as u32) << 8 | self[2] as u32
    }
}

macro_rules! basic_impl {
    ($n:ident, $t:ident) => {
        pub fn $n(mut self, $t: impl ToString) -> Self {
            self.embed.$t($t);
            self
        }
    };
}

impl SendEmbedBuilder<'_> {
    #[inline]
    pub fn new(http: Arc<Http>, channel: ChannelId) -> Self {
        Self {
            http: Some(http),
            channel: channel.0,
            embed: CreateEmbed::default(),
            fut: None,
        }
    }

    basic_impl!(with_title, title);
    basic_impl!(with_url, url);
    basic_impl!(with_attachment, attachment);
    basic_impl!(with_description, description);
    basic_impl!(with_image, image);
    basic_impl!(with_thumbnail, thumbnail);

    pub fn with_field(mut self, name: impl ToString, value: impl ToString, inline: bool) -> Self {
        self.embed.field(name, value, inline);
        self
    }

    pub fn with_fields<N, S, I>(mut self, iter: I) -> Self
    where
        N: ToString,
        S: ToString,
        I: IntoIterator<Item = (N, S, bool)>,
    {
        self.embed.fields(iter);
        self
    }

    pub fn with_random_color(self) -> Self {
        self.with_color(SmallRng::from_entropy().next_u32())
    }

    pub fn random_color(&mut self) -> &mut Self {
        self.embed.color(SmallRng::from_entropy().next_u32());
        self
    }

    pub fn with_color(mut self, color: impl Color) -> Self {
        self.embed.color(color.to_number());
        self
    }

    #[inline]
    pub fn with_current_timestamp(self) -> Self {
        self.with_timestamp(&chrono::Utc::now())
    }

    pub fn current_timestamp(&mut self) -> &mut Self {
        self.embed.timestamp(&chrono::Utc::now());
        self
    }

    pub fn with_timestamp(mut self, timestamp: impl Into<Timestamp>) -> Self {
        self.embed.timestamp(timestamp);
        self
    }

    pub fn with_author(
        mut self,
        name: impl ToString,
        url: impl ToString,
        icon_url: impl ToString,
    ) -> Self {
        self.embed
            .author(|f| f.name(name).url(url).icon_url(icon_url));
        self
    }

    pub fn with_author_name(mut self, name: impl ToString) -> Self {
        self.embed.author(|f| f.name(name));
        self
    }

    pub fn with_author_url(mut self, url: impl ToString) -> Self {
        self.embed.author(|f| f.url(url));
        self
    }

    pub fn with_author_icon(mut self, icon_url: impl ToString) -> Self {
        self.embed.author(|f| f.icon_url(icon_url));
        self
    }

    pub fn with_footer(mut self, text: impl ToString, url: impl ToString) -> Self {
        self.embed.footer(|f| f.text(text).icon_url(url));
        self
    }

    pub fn with_footer_text(mut self, text: impl ToString) -> Self {
        self.embed.footer(|f| f.text(text));
        self
    }

    pub fn with_footer_icon(mut self, url: impl ToString) -> Self {
        self.embed.footer(|f| f.icon_url(url));
        self
    }

    pub fn with_embedable_object(mut self, obj: impl Embedable) -> Self {
        obj.append_to(&mut self.embed);
        self
    }

    #[inline]
    pub fn inner_embed(&mut self) -> &mut CreateEmbed {
        &mut self.embed
    }
}

impl Future for SendEmbedBuilder<'_> {
    type Output = Result<Message>;

    fn poll(mut self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        match self.fut.as_mut() {
            Some(ref mut future) => future.as_mut().poll(ctx),
            None => {
                let http = match self.http.take() {
                    Some(h) => h,
                    None => {
                        return Poll::Ready(Err(
                            "Http notfound, consider use the `send` method instead".into(),
                        ))
                    }
                };

                let channel = self.channel;
                let embed = mem::take(&mut self.embed.0);
                let embed = serenity::utils::hashmap_to_json_map(embed);

                let mut content: Map<String, Value> = Map::new();
                content.insert(String::from("embed"), embed.into());

                self.fut = Some(Box::pin(async move {
                    http.send_message(channel, &content.into())
                        .await
                        .map_err(|e| e.into())
                }));

                self.fut.as_mut().unwrap().as_mut().poll(ctx)
            }
        }
    }
}

use serenity::builder::EditMessage;

pub struct EditMessageBuilder<'a> {
    http: Option<Arc<Http>>,
    channel: u64,
    message: u64,
    data: EditMessage,
    fut: Option<BoxedFuture<'a, Result<Message>>>,
}

impl Deref for EditMessageBuilder<'_> {
    type Target = EditMessage;

    fn deref(&self) -> &Self::Target {
        &self.data
    }
}

impl DerefMut for EditMessageBuilder<'_> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.data
    }
}

impl EditMessageBuilder<'_> {
    #[inline]
    pub fn new(http: Arc<Http>, channel: ChannelId, message: MessageId) -> Self {
        Self {
            http: Some(http),
            channel: channel.0,
            message: message.0,
            data: Default::default(),
            fut: None,
        }
    }

    pub fn with_content(mut self, content: impl ToString) -> Self {
        self.data.content(content);
        self
    }

    pub fn with_embed(mut self, embed: impl Embedable) -> Self {
        self.data.embed(|e| embed.append_to(e));
        self
    }

    pub fn with_suppress_embeds(mut self, suppress_embeds: bool) -> Self {
        self.data.suppress_embeds(suppress_embeds);
        self
    }
}

impl Future for EditMessageBuilder<'_> {
    type Output = Result<Message>;

    fn poll(mut self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        match self.fut.as_mut() {
            Some(ref mut future) => future.as_mut().poll(ctx),
            None => {
                let http = match self.http.take() {
                    Some(h) => h,
                    None => {
                        return Poll::Ready(Err(
                            "Http notfound, consider use the `send` method instead".into(),
                        ))
                    }
                };

                let channel = self.channel;
                let message_id = self.message;
                let data = mem::take(&mut self.data.0);
                let map = serenity::utils::hashmap_to_json_map(data);

                self.fut = Some(Box::pin(async move {
                    http.edit_message(channel, message_id, &Value::Object(map))
                        .await
                        .map_err(|e| e.into())
                }));

                self.fut.as_mut().unwrap().as_mut().poll(ctx)
            }
        }
    }
}
