#[macro_use]
extern crate async_trait;

use std::error::Error;

pub(crate) type Result<T> = core::result::Result<T, Box<dyn Error + Send + Sync>>;

pub mod channelext;
pub mod embedable;
pub mod events;
pub mod paginator;

pub use channelext::{ChannelExt, Color};
pub use embedable::Embedable;
pub use events::{MultiRawHandler, RawEventHandlerRef};
pub use paginator::{Paginator, PaginatorOption};

pub use serenity::builder::CreateEmbed;
