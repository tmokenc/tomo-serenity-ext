pub use crate::CreateEmbed;
use serenity::http::client::Http;
use serenity::model::channel::Message;
use serenity::model::id::ChannelId;

/// This trait exist due to the number of rewriting thanks to my stupid code
#[async_trait]
pub trait Embedable: Send {
    fn append(&self, embed: &mut CreateEmbed);
    fn append_to<'a>(&self, embed: &'a mut CreateEmbed) -> &'a mut CreateEmbed {
        self.append(embed);
        embed
    }

    fn embed_data(&self) -> CreateEmbed {
        let mut embed = CreateEmbed::default();
        self.append(&mut embed);
        embed
    }

    async fn send_embed<H: AsRef<Http> + Send + Sync>(
        &self,
        http: H,
        channel_id: ChannelId,
    ) -> serenity::Result<Message> {
        channel_id
            .send_message(http, |m| m.embed(|e| self.append_to(e)))
            .await
    }
}

impl Embedable for CreateEmbed {
    fn append(&self, embed: &mut CreateEmbed) {
        embed.0 = self.0.to_owned();
    }

    #[inline]
    fn embed_data(&self) -> CreateEmbed {
        self.to_owned()
    }
}

impl Embedable for String {
    fn append(&self, embed: &mut CreateEmbed) {
        embed.description(self);
    }
}

impl Embedable for &str {
    fn append(&self, embed: &mut CreateEmbed) {
        embed.description(self);
    }
}
