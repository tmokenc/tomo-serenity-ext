use futures::future;
pub use serenity::model::event::Event;
use serenity::prelude::*;
use std::collections::HashMap;
use std::sync::Arc;
use tokio::sync::RwLock;

#[async_trait]
pub trait RawEventHandlerRef: Send + Sync {
    async fn raw_event_ref(&self, ctx: &Context, ev: &Event);
}

#[async_trait]
impl RawEventHandler for dyn RawEventHandlerRef {
    async fn raw_event(&self, ctx: Context, ev: Event) {
        self.raw_event_ref(&ctx, &ev).await
    }
}

#[async_trait]
impl RawEventHandler for MultiRawHandler {
    async fn raw_event(&self, ctx: Context, ev: Event) {
        self.execute(ctx, ev).await;
    }
}

#[derive(Clone)]
pub struct MultiRawHandler {
    events: Arc<RwLock<HashMap<String, Arc<dyn RawEventHandlerRef>>>>,
}

impl MultiRawHandler {
    pub fn new() -> Self {
        Self {
            events: Arc::new(RwLock::new(HashMap::new())),
        }
    }

    pub async fn add<H: RawEventHandlerRef + 'static>(&self, name: impl ToString, handler: H) {
        let name = name.to_string();
        let handler = Arc::new(handler) as Arc<_>;
        self.events.write().await.insert(name, handler);
    }

    pub async fn remove(&self, name: impl ToString) {
        let name = name.to_string();
        self.events.write().await.remove(&name);
    }

    pub async fn execute(&self, ctx: Context, ev: Event) {
        let handlers = self.events.read().await;
        let futs = handlers.values().cloned().collect::<Vec<_>>();

        drop(handlers);

        let futs = futs.iter().map(|v| v.raw_event_ref(&ctx, &ev));
        future::join_all(futs).await;
    }
}
