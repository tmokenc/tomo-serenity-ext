pub use crate::CreateEmbed;
use crate::{ChannelExt, Embedable, Result};
use core::borrow::Borrow;
use core::convert::TryFrom;
pub use core::num::NonZeroUsize;
use core::str::FromStr;
use core::time::Duration;
use futures::stream::StreamExt;
use magic::types::Void;
use serenity::client::Context;
use serenity::collector::reaction_collector::ReactionCollectorBuilder;
use serenity::model::channel::{Message, ReactionType};
use serenity::model::id::{ChannelId, UserId};
use std::env;
use std::sync::Arc;
use tokio::time::timeout;

#[derive(Debug, Clone)]
pub struct PaginatorReactions {
    reactions: [ReactionType; 5],
}

#[derive(Debug, Clone, Copy)]
pub enum PaginatorAction {
    Page(usize),
    First,
    Previous,
    Next,
    Last,
    Destroy,
}

impl FromStr for PaginatorAction {
    type Err = Void;
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let res = match s.to_lowercase().trim() {
            "first" => Self::First,
            "previous" => Self::Previous,
            "next" => Self::Next,
            "last" => Self::Last,
            s if s.starts_with("page") => Self::Page(s[4..].trim().parse()?),
            _ => return Err(Void),
        };

        Ok(res)
    }
}

impl Default for PaginatorReactions {
    #[rustfmt::skip]
    fn default() -> Self {
        let reactions = if let Ok(reactions) = env::var("PAGINATOR_REACTIONS") {
            use ReactionType::Unicode;
            let mut iter = reactions.split(',').map(ReactionType::try_from);

            [
                iter.next().and_then(|v| v.ok()).unwrap_or_else(|| Unicode(String::from("⏮️"))),
                iter.next().and_then(|v| v.ok()).unwrap_or_else(|| Unicode(String::from("◀️"))),
                iter.next().and_then(|v| v.ok()).unwrap_or_else(|| Unicode(String::from("▶️"))),
                iter.next().and_then(|v| v.ok()).unwrap_or_else(|| Unicode(String::from("⏭️"))),
                iter.next().and_then(|v| v.ok()).unwrap_or_else(|| Unicode(String::from("❌"))),
            ]
        } else {
            [
                ReactionType::Unicode(String::from("⏮️")),
                ReactionType::Unicode(String::from("◀️")),
                ReactionType::Unicode(String::from("▶️")),
                ReactionType::Unicode(String::from("⏭️")),
                ReactionType::Unicode(String::from("❌")),
            ]
        };

        Self { reactions }
    }
}

impl PaginatorReactions {
    #[inline]
    pub fn iter(&self) -> impl Iterator<Item = &ReactionType> {
        self.reactions.iter()
    }

    #[allow(dead_code)]
    pub fn change(&mut self, kind: PaginatorAction, reaction: ReactionType) {
        use PaginatorAction::*;

        let current = match kind {
            First => &mut self.reactions[0],
            Previous => &mut self.reactions[1],
            Next => &mut self.reactions[2],
            Last => &mut self.reactions[3],
            Destroy => &mut self.reactions[4],
            _ => return,
        };

        *current = reaction;
    }

    #[inline]
    pub fn kind(&self, reaction: &ReactionType) -> Option<PaginatorAction> {
        use PaginatorAction::*;

        self.iter()
            .position(|ref v| v == &reaction)
            .and_then(|v| match v {
                0 => Some(First),
                1 => Some(Previous),
                2 => Some(Next),
                3 => Some(Last),
                4 => Some(Destroy),
                _ => None,
            })
    }
}

pub struct PaginatorOption {
    pub channel_id: ChannelId,
    pub user: UserId,
}

impl PaginatorOption {
    #[inline]
    pub fn new(channel_id: ChannelId, user: UserId) -> Self {
        Self { channel_id, user }
    }
}

impl From<&Message> for PaginatorOption {
    fn from(m: &Message) -> Self {
        Self {
            channel_id: m.channel_id,
            user: m.author.id,
        }
    }
}

#[async_trait]
pub trait Paginator {
    /// Notice that the page start at 1
    fn append_page(&self, page: NonZeroUsize, embed: &mut CreateEmbed);

    #[inline]
    fn default_page(&self) -> NonZeroUsize {
        unsafe { NonZeroUsize::new_unchecked(1) }
    }

    #[inline]
    fn total_pages(&self) -> Option<usize> {
        None
    }

    #[inline]
    fn timeout(&self) -> Duration {
        Duration::from_secs(30)
    }

    #[inline]
    fn reactions(&self) -> PaginatorReactions {
        Default::default()
    }

    async fn pagination<C, O>(&self, ctx: C, opt: O) -> Result<()>
    where
        C: Borrow<Context> + Send,
        O: Into<PaginatorOption> + Send,
        Self: Send + Sync,
    {
        let ctx = ctx.borrow();
        let opt = opt.into();
        let total = self.total_pages();

        let http = Arc::clone(&ctx.http);

        match total {
            Some(0) => return Ok(()),
            Some(1) => {
                let page = unsafe { NonZeroUsize::new_unchecked(1) };

                let mut send_embed = opt.channel_id.send_embed(ctx);
                self.append_page(page, send_embed.inner_embed());

                send_embed.await?;

                return Ok(());
            }
            _ => (),
        };

        let reactions = self.reactions();
        let mut current_page = self.default_page().get();

        let mess = ChannelExt::send_message(&opt.channel_id, ctx)
            .with_reactions(reactions.iter().cloned())
            .with_embed({
                let page = NonZeroUsize::new(current_page).unwrap();
                let mut embed = CreateEmbed::default();
                self.append_page(page, &mut embed);
                embed
            })
            .await?;

        let channel = mess.channel_id;
        let message = mess.id;

        drop(mess);

        let react_collector = ReactionCollectorBuilder::new(ctx)
            .message_id(message)
            .channel_id(channel)
            .author_id(opt.user)
            .await
            .filter_map(|reaction| {
                let reaction = reaction.as_inner_ref().to_owned();
                let result = reactions.kind(&reaction.emoji);

                async move {
                    if result.is_some() {
                        let http = Arc::clone(&ctx.http);
                        tokio::spawn(async move {
                            reaction.delete(http).await.ok();
                        });
                    }

                    result
                }
            });

        // let msg_collector = opt
        //     .channel_id
        //     .await_replies(ctx)
        //     .author_id(opt.user)
        //     .await
        //     .filter_map(|v| async move { v.content.parse::<PaginatorAction>().ok() });

        // let stream = futures::stream::select(react_collector, msg_collector);
        let stream = react_collector;
        futures::pin_mut!(stream);

        while let Ok(Some(action)) = timeout(self.timeout(), stream.next()).await {
            match action {
                PaginatorAction::First if current_page > 1 => current_page = 1,

                PaginatorAction::Previous if current_page > 1 => current_page -= 1,

                PaginatorAction::Next => match total {
                    Some(max) if current_page < max => current_page += 1,
                    _ => continue,
                },

                PaginatorAction::Last => match total {
                    Some(max) if current_page != max => current_page = max,
                    _ => continue,
                },

                PaginatorAction::Page(page) if current_page != page => match total {
                    Some(max) if page <= max => current_page = page,
                    _ => current_page = page,
                },

                PaginatorAction::Destroy => {
                    http.delete_message(channel.0, message.0).await?;
                    return Ok(());
                }

                _ => continue,
            }

            let page = NonZeroUsize::new(current_page).unwrap();

            let mut embed = CreateEmbed::default();
            self.append_page(page, &mut embed);
            channel
                .0
                .edit_message(ctx, message)
                .with_embed(embed)
                .await?;
        }

        drop(stream);
        drop(ctx);

        tokio::spawn(async move {
            let http = http;

            let futs = reactions
                .iter()
                .cloned()
                .map(|s| opt.channel_id.delete_reaction(&http, message.0, None, s));

            futures::future::join_all(futs).await;
        });
        Ok(())
    }
}

impl<E: Embedable> Paginator for Vec<E> {
    fn append_page(&self, page: NonZeroUsize, embed: &mut CreateEmbed) {
        let page = page.get();
        embed.footer(|f| f.text(format!("{} / {}", page, self.len())));
        match self.get(page - 1) {
            Some(data) => data.append_to(embed),
            None => embed.description("This page does not exist"),
        };
    }

    #[inline]
    fn total_pages(&self) -> Option<usize> {
        Some(self.len())
    }
}

#[async_trait]
impl<P: Paginator> Paginator for tokio::sync::Mutex<P> {
    fn append_page(&self, _page: NonZeroUsize, _embed: &mut CreateEmbed) {}

    async fn pagination<C, O>(&self, ctx: C, opt: O) -> Result<()>
    where
        C: Borrow<Context> + Send,
        O: Into<PaginatorOption> + Send,
        Self: Send + Sync,
    {
        let ctx = ctx.borrow();
        let opt = opt.into();
        let data = self.lock().await;
        let http = Arc::clone(&ctx.http);

        let total = data.total_pages();

        match total {
            Some(0) => return Ok(()),
            Some(1) => {
                let page = unsafe { NonZeroUsize::new_unchecked(1) };

                let mut send_embed = opt.channel_id.send_embed(ctx);
                data.append_page(page, send_embed.inner_embed());
                drop(data);

                send_embed.await?;

                return Ok(());
            }
            _ => (),
        };

        let reactions = data.reactions();
        let mut current_page = data.default_page().get();

        let embed = {
            let page = NonZeroUsize::new(current_page).unwrap();
            let mut embed = CreateEmbed::default();
            data.append_page(page, &mut embed);
            embed
        };

        let next_timeout = data.timeout();

        drop(data);

        let mess = ChannelExt::send_message(&opt.channel_id, ctx)
            .with_reactions(reactions.iter().cloned())
            .with_embed(embed)
            .await?;

        let channel = mess.channel_id;
        let message = mess.id;

        drop(mess);

        let react_collector = ReactionCollectorBuilder::new(ctx)
            .channel_id(channel)
            .message_id(message)
            .author_id(opt.user)
            .await
            .filter_map(|reaction| {
                let reaction = reaction.as_inner_ref().to_owned();
                let result = reactions.kind(&reaction.emoji);

                async move {
                    if result.is_some() {
                        let http = Arc::clone(&ctx.http);
                        tokio::spawn(async move {
                            reaction.delete(http).await.ok();
                        });
                    }

                    result
                }
            });

        // let msg_collector = opt
        //     .channel_id
        //     .await_replies(ctx)
        //     .author_id(opt.user)
        //     .await
        //     .filter_map(|v| async move { v.content.parse::<PaginatorAction>().ok() });

        // let stream = futures::stream::select(react_collector, msg_collector);
        let stream = react_collector;
        futures::pin_mut!(stream);

        while let Ok(Some(action)) = timeout(next_timeout, stream.next()).await {
            match action {
                PaginatorAction::First if current_page > 1 => current_page = 1,

                PaginatorAction::Previous if current_page > 1 => current_page -= 1,

                PaginatorAction::Next => match total {
                    Some(max) if current_page < max => current_page += 1,
                    _ => continue,
                },

                PaginatorAction::Last => match total {
                    Some(max) if current_page != max => current_page = max,
                    _ => continue,
                },

                PaginatorAction::Page(page) if current_page != page => match total {
                    Some(max) if page <= max => current_page = page,
                    _ => current_page = page,
                },

                PaginatorAction::Destroy => {
                    http.delete_message(channel.0, message.0).await?;
                    return Ok(());
                }

                _ => continue,
            }

            let page = NonZeroUsize::new(current_page).unwrap();
            let mut embed = CreateEmbed::default();
            self.lock().await.append_page(page, &mut embed);

            channel
                .0
                .edit_message(ctx, message)
                .with_embed(embed)
                .await?;
        }

        drop(stream);
        drop(ctx);

        tokio::spawn(async move {
            let http = http;
            let futs = reactions
                .iter()
                .cloned()
                .map(|s| opt.channel_id.delete_reaction(&http, message.0, None, s));

            futures::future::join_all(futs).await;
        });

        Ok(())
    }
}
